# zbx

Docker zabbix deployment

1. Create docker file to:
1.1 take latest centos image
1.2 install ansible and run with playbook from repo


2. Ansible (zbx_srv)playbook from 1.2 should do:
2.1 Download and install latest zabbix server
2.2 Configure zabbix with DB variables

3. Ansible (zbx_web)playbook from 1.2 should do:
3.1 Download and install latest zabbix web
3.2 Configure zabbix with DB variables

4. Ansible (zbx_images)playbook should do:
4.1 Prepare steps 1-3 through ansible 
4.2 Use docker to build images from dockerfiles for zbx_srv and zbx_web 