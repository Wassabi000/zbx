FROM ansible/centos7-ansible:stable

RUN ansible-pull -U https://gitlab.com/Wassabi000/zbx/-/blob/master/tasks/zbx_srv_playbook.yml


#Moved to Ansible playbook
# RUN yum install epel-release -y \
#     && rpm --import http://repo.zabbix.com/RPM-GPG-KEY-ZABBIX \
#     && rpm -Uv  http://repo.zabbix.com/zabbix/2.4/rhel/7/x86_64/zabbix-release-2.4-1.el7.noarch.rpm \
#     && yum install -y zabbix-server-mysql zabbix-web-mysql zabbix-agent zabbix-java-gateway 

# Is this even necessary?
# RUN yum install firewall-d -y \
#     && firewall-cmd --permanent --add-port=10050/tcp \
#     && firewall-cmd --permanent --add-port=10051/tcp 
