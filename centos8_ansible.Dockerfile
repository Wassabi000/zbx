FROM centos:latest

RUN dnf install python3 && \
    subscription-manager repos --enable ansible-2.8-for-rhel-8-x86_64-rpms && \
    dnf -y install ansible
# get playbook and run it
